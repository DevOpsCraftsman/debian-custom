#!/bin/bash
utilisateur=$USER
cp -R /etc/skel/. /home/$utilisateur/
chown $utilisateur:$utilisateur -R /home/$utilisateur/
apt-get autoremove --purge live-boot live-config live-config-systemd
